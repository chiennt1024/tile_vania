using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float runSpeed = 5F;
    [SerializeField] float jumpSpeed = 25F;
    [SerializeField] float climbSpeed = 5F;
    [SerializeField] Vector2 deathKick = new Vector2(20F, 20F);
    [SerializeField] GameObject bullet;
    [SerializeField] Transform gun;
    Vector2 moveInput;
    Rigidbody2D _rigidbody2D;
    Animator _animator;
    CapsuleCollider2D _capsuleCollider2D;
    BoxCollider2D _boxCollider2D;
    float gravityScaleAtStart;
    bool isAlive = true;

    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _capsuleCollider2D = GetComponent<CapsuleCollider2D>();
        _boxCollider2D = GetComponent<BoxCollider2D>();
        gravityScaleAtStart = _rigidbody2D.gravityScale;
        isAlive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(!isAlive) {
            return;
        }
        Run();
        FlipSprite();
        ClimbLadder();
        Die();
    }

    void OnFire(InputValue value) {
        if(!isAlive) {
            return;
        }
        Instantiate(bullet, gun.position, transform.rotation);
    }

    void OnMove(InputValue value)
    {
        if(!isAlive) {
            return;
        }
        moveInput = value.Get<Vector2>();
    }

    void OnJump(InputValue value)
    {
        if(!isAlive) {
            return;
        }
        if (!_boxCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            return;
        }

        if (value.isPressed)
        {
            _rigidbody2D.velocity += new Vector2(0F, jumpSpeed);
        }
    }

    void Run()
    {
        Vector2 playerVelocity = new Vector2(moveInput.x * runSpeed, _rigidbody2D.velocity.y);
        _rigidbody2D.velocity = playerVelocity;
        bool playHasHorizontalSpeed = Mathf.Abs(_rigidbody2D.velocity.x) > Mathf.Epsilon;
        _animator.SetBool("isRunning", playHasHorizontalSpeed);
    }

    void FlipSprite()
    {
        bool playHasHorizontalSpeed = Mathf.Abs(_rigidbody2D.velocity.x) > Mathf.Epsilon;

        if (playHasHorizontalSpeed)
        {
            transform.localScale = new Vector2(Mathf.Sign(_rigidbody2D.velocity.x), 1F);
        }

    }

    void ClimbLadder() {
        if (!_boxCollider2D.IsTouchingLayers(LayerMask.GetMask("Climbing")))
        {
            _rigidbody2D.gravityScale = gravityScaleAtStart;
            return;
        }
        bool playHasVerticalSpeed = Mathf.Abs(_rigidbody2D.velocity.y) > Mathf.Epsilon;
        Vector2 climbVelocity = new Vector2(_rigidbody2D.velocity.x, moveInput.y * climbSpeed);
        _animator.SetBool("isClimbing", playHasVerticalSpeed);

        _rigidbody2D.velocity = climbVelocity;
        _rigidbody2D.gravityScale = 0F;
    }

    void Die() {
        if(_capsuleCollider2D.IsTouchingLayers(LayerMask.GetMask("Enemy", "Hazard"))) {
            isAlive = false;
            _animator.SetTrigger("Dying");
            _rigidbody2D.velocity = deathKick;
            FindObjectOfType<GameSession>().ProcessPlayDeath();
        }
    }
}
