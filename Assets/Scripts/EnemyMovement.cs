using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] float moveSpeed = 1F;
    Rigidbody2D _rigidBody2D;
    void Start()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _rigidBody2D.velocity = new Vector2(moveSpeed, 0F);
    }

    void OnTriggerExit2D(Collider2D other) {
        moveSpeed = -moveSpeed;
        FlipEnemyFacing();
    }

    void FlipEnemyFacing() {
        transform.localScale = new Vector2(-(Mathf.Sign(_rigidBody2D.velocity.x)), 1F);
    }
}
